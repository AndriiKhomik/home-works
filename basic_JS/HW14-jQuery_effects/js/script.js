$(document).ready(function() {

    $(function(){
        $('.design').on('click', function(e){
            $('html,body').stop().animate({ scrollTop: $('#design').offset().top }, 1000);
            e.preventDefault();
        });

        $('.our-clients').on('click', function(e){
            $('html,body').stop().animate({ scrollTop: $('#clients').offset().top }, 1000);
            e.preventDefault();
        });

        $('.rated').on('click', function(e){
            $('html,body').stop().animate({ scrollTop: $('#rated').offset().top }, 1000);
            e.preventDefault();
        });

        $('.hot-news').on('click', function(e){
            $('html,body').stop().animate({ scrollTop: $('#news').offset().top }, 1000);
            e.preventDefault();
        });
    });

    let btn = $('#toTop');
    $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
            btn.removeClass('hidden');
        } else {
            btn.addClass('hidden');
        }
    });
    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '400');
    });

    $('#hideItem').click(function() {
        $( '.posts-items' ).slideToggle( "hide" );
    });
});
