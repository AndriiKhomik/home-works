
const button = document.querySelector('.btn');

function primaryInput() {
    const primary = document.getElementById('primaryInput');
    const showPasswordIcon = primary.querySelector('.fa-eye');
    const hidePasswordIcon = primary.querySelector('.fa-eye-slash');

    primary.addEventListener('click', (event) => {
        if (event.target.classList.contains('fa-eye')) {
            showPasswordIcon.classList.add('hidden');
            hidePasswordIcon.classList.remove('hidden');
            primary.querySelector('input').setAttribute('type', 'text');
        } else if (event.target.classList.contains('fa-eye-slash')) {
            showPasswordIcon.classList.remove('hidden');
            hidePasswordIcon.classList.add('hidden');
            primary.querySelector('input').setAttribute('type', 'password');
        }
    });
}
primaryInput();

function secondaryInput() {
    const secondary = document.getElementById('secondaryInput');
    const showPasswordIcon = secondary.querySelector('.fa-eye');
    const hidePasswordIcon = secondary.querySelector('.fa-eye-slash');

    secondary.addEventListener('click', (event) => {
        if (event.target.classList.contains('fa-eye')) {
            showPasswordIcon.classList.add('hidden');
            hidePasswordIcon.classList.remove('hidden');
            secondary.querySelector('input').setAttribute('type', 'text');
        } else if (event.target.classList.contains('fa-eye-slash')) {
            showPasswordIcon.classList.remove('hidden');
            hidePasswordIcon.classList.add('hidden');
            secondary.querySelector('input').setAttribute('type', 'password');
        }
    });
}
secondaryInput();

button.addEventListener('click', (event) => {
    event.preventDefault();

    const primary = document.getElementById('primaryInputValue').value;
    const secondary = document.getElementById('secondaryInputValue').value;

    if (primary === secondary) {
        document.querySelector('.modal-success').style.top = '15px';
    } else {
        document.querySelector('.modal-error').style.top = '15px';
    }
});



