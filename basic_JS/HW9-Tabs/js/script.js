const tabs = document.querySelector('.tabs');
const tabsItems = document.querySelectorAll('.tabs-title');
const tabsContent = document.querySelectorAll('.tabs-content li');

function hideTabsContent() {
    tabsContent.forEach(item => {
        item.classList.add('hide');
        item.classList.remove('show');
    });

    tabsItems.forEach(item => {
        item.classList.remove('active');
    });
}

function showTabsContent(i = 0) {
    tabsContent[i].classList.add('show');
    tabsContent[i].classList.remove('hide');
    tabsItems[i].classList.add('active');
}

hideTabsContent();
showTabsContent();

tabs.addEventListener('click', (event) => {
    if (event.target && event.target.classList.contains('tabs-title')) {
        tabsItems.forEach((item, i) => {
            if (event.target === item) {
                hideTabsContent();
                showTabsContent(i);
            }
        });
    }
});


