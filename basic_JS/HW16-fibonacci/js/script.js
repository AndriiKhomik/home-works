let getNumber = +prompt('Please type any number');

function fib(f0, f1, n) {

    for (let i = 3; i <= n; i++) {
        let f2 = f0 + f1;
        f0 = f1;
        f1 = f2;
    }
    return f1
}

const fibResult = fib(1, 1, getNumber);

console.log(fibResult);

