let number = prompt('Please type any number');
while (!isValidNumber(number)) {
    number = prompt('Please type any number', `${number}`);
}

function factorial(a) {
    let sum = null;
    if (a) {
        sum = a * factorial (a - 1);
        return sum;
    } else {
        return 1;
    }
}

function isValidNumber(value) {
    if (value === '' || value === null || Number.isNaN(+value)) {
        return false;
    }
    return true;
}

alert(factorial(number));