let firstName = prompt('Please type your name');
while (!isValid(firstName)) {
    firstName = prompt('Please type your name');
}

let lastName = prompt('Please type your last name');
while (!isValid(lastName)) {
    lastName = prompt('Please type your last name');
}

let userBirthDate = prompt('When were you born?', 'dd.mm.yyyy');

function isValid(value) {
    return (!(value === '' || value === null));
}

function createNewUser(firstName, lastName, birthday) {
    const newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return this.firstName.slice(0, 1).toLowerCase()+this.lastName.toLowerCase();
        },
        getAge() {
            const date = new Date();

            let day = this.birthday.slice(0, 2);
            let month = this.birthday.slice(3, 5);
            let year = this.birthday.slice(6);
            let age = null;

            if (month > date.getMonth()) {
                year = +year + 1
            }

            age = (date.getDate() - day, date.getMonth() - month, date.getFullYear() - year);
            return age
        },
        getPassword() {
            return this.firstName.slice(0, 1).toUpperCase()+this.lastName.toLowerCase()+this.birthday.slice(6)

        }
    };
    return newUser
}

const firstUser = createNewUser(firstName, lastName, userBirthDate);
console.log(firstUser.getAge());
console.log(firstUser.getPassword());

