const greetingCities = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

function innerArray(array, domElem =  document.body) {

    if (typeof domElem === 'string') {
        const element = document.createElement(`${domElem}`);

        document.body.prepend(element);

        array.map((e) => {
            if (typeof e === "object") {
                const insideList = document.createElement(`${domElem}`);
                element.append(insideList);

                e.map((j) => {
                    const item = document.createElement('li');
                    item.classList.add('item-list');
                    item.innerHTML = j;
                    insideList.append(item);
                })
            } else  if (typeof e !== "object") {
                const li = document.createElement('li');
                li.classList.add('item-list');
                li.innerHTML = e;
                element.append(li);
            }
        });
    } else {
        array.map((e) => {
            const li = document.createElement('li');
            li.classList.add('item-list');
            li.innerHTML = e;
            document.body.prepend(li);
        });
    }
}

innerArray(greetingCities, 'ol');

async function removeElement() {
    await (setTimeout(alertData, 5000));
}

function alertData() {
    document.body.innerHTML = '';
    setTimeout(() => document.body.innerHTML = 'All content from the page has been removed', 2000)
}

removeElement();

function printNumbers(from) {
    let current = from;
    const div = document.createElement('div');
    div.classList.add('timer');
    div.innerHTML = `00:00:0${current}`;
    document.body.append(div);

    let timerId = setInterval(function() {
        document.body.append(div);
        current--;
        if (current === 0) {
            clearInterval(timerId);
        }
        div.innerHTML = `00:00:0${current}`;
    }, 1000);
}

printNumbers(4);








