// const greetingCities = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// const someValuesArray = ["1", "2", "3", "sea", "user", 23];
//
// function innerArray(array, domElem =  document.body) {
//
//     if (typeof domElem === 'string') {
//         const element = document.createElement(`${domElem}`);
//
//         document.body.prepend(element);
//
//         array.map((e) => {
//             const li = document.createElement('li');
//             li.classList.add('item-list');
//             li.innerHTML = e;
//             element.append(li);
//         });
//     } else {
//         array.map((e) => {
//             const li = document.createElement('li');
//             li.classList.add('item-list');
//             li.innerHTML = e;
//             document.body.prepend(li);
//         });
//     }
// }
//
// innerArray(greetingCities, 'ul');
// innerArray(someValuesArray, 'ol');
// innerArray(greetingCities);
//
//
"use strict";
let array = [
    "hello",
    "world",
    "Kiev",
    ["Borispol", "Irpin"],
    "Kharkiv",
    "Odessa",
    "Lviv",
];
let parentt = document.body;
function displayAList(array, elem) {
    let ul = document.createElement("ul");
    // ВСТАВЛЯЕМ КОНТЕНТ В УЛ
    ul.innerHTML = createList(array);
    // ВСТАВЛЯЕМ УЛ НА СТРАНИЦУ
    elem.append(ul);
}
function createList(array) { // СОЗДАЕМ ФУНКЦИЮ ДЛЯ СОЗДАНИЯ КОНТЕНТА
    return array.map((arrItem) => { //ФУНКЦИЯ ВОЗВРАЩАЕТ МАССИВ
        if (Array.isArray(arrItem)) { //ЕСЛИ ЭЛМЕНТ ТОЖЕ МАССИВ - РЕКУРСИЯ
            return `<li><ul>${createList(arrItem)}</ul></li>`; //ВОЗВРАЩАЕМ ХТМЛ СТРОКОЙ ШАБЛОННОЙ
        } else {
            return `<li>${arrItem}</li>`; //ТОЖЕ
        }
    }).join(""); //СОЕДИНЯЕМ В СТРОКУ
}
displayAList(array, parentt);
