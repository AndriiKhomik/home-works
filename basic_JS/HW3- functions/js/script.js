function calculationsProgram() {

    let firstNumber = prompt('Please type first number');
    while (!isValidNumber(firstNumber)) {
        firstNumber = prompt('Please check input, first number is expected', `${firstNumber}`);
    }
    let operation = prompt('Please type operation which one you want to do');
    while (!isValidOperation(operation)) {
        operation = prompt('Error, you can just type symbols like + - * /', `${operation}`);
    }

    let secondNumber = prompt('Please type second number');
    while (!isValidNumber(secondNumber)) {
        secondNumber = prompt('Please check input, second number is expected', `${secondNumber}`);
    }

    console.log(calc(firstNumber, secondNumber, operation));
}

function isValidNumber(value) {
    if (value === "" || value === null || Number.isNaN(+value)) {
        return false;
    }
    return true;
}

function isValidOperation(value) {
    if (value === "+" || value === "-" || value === "*" || value === "/") {
        return true;
    }
    return;
}

function calc(a, b, symbol) {
    let res = null;
    if (symbol === '+') {
        res = a + b;
        return res;
    } if (symbol === '-') {
        res = a - b;
        return res;
    } if (symbol === '*') {
        res = a * b;
        return res;
    } if (symbol === '/') {
        res = a / b;
        return res;
    }
}

calculationsProgram();
