const images = document.querySelectorAll('.image-to-show');

function createContinueButton() {
    const button = document.createElement('button');
    button.classList.add('btn-continue');
    button.innerHTML = 'Возобновить показ';
    document.body.prepend(button)
}
createContinueButton();

function createStopButton() {
    const button = document.createElement('button');
    button.classList.add('btn-stop');
    button.innerHTML = 'Прекратить';
    document.body.prepend(button)
}
createStopButton();

let slideIndex = 1;
showImage(slideIndex);

let timer = setInterval(function timer() {
    nextImage(1);
}, 3000);

function nextImage(n) {
    showImage(slideIndex += n);
}

function showImage(n) {

    if (n > images.length) {
        slideIndex = 1
    }

    for (let i = 0; i < images.length; i++) {
        images[i].style.display ="none";
    }
    images[slideIndex-1].style.display = "block";
}

const stopButton = document.querySelector('.btn-stop');
const continueButton = document.querySelector('.btn-continue');

continueButton.addEventListener('click', () => {
    timer = setInterval(function timer() {
        nextImage(1);
    }, 3000);
});

stopButton.addEventListener('click', () => {
    clearInterval(timer);
});









