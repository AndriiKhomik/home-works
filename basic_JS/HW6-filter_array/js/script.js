const array =  ['hello', 'world', 23, '23', null];

function filterBy(arr, typeOfData) {
    const transformedArray = [];
        arr.forEach((element) => {
        if (typeof(element) !== typeOfData) {
            transformedArray.push(element);
        }
    });
    return transformedArray;
}

const res = filterBy(array, 'string');
console.log(res);