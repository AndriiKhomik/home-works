const enter = document.querySelector('.btn:nth-child(1)');
const keyS = document.querySelector('.btn:nth-child(2)');
const keyE = document.querySelector('.btn:nth-child(3)');
const keyO = document.querySelector('.btn:nth-child(4)');
const keyN = document.querySelector('.btn:nth-child(5)');
const keyL = document.querySelector('.btn:nth-child(6)');
const keyZ = document.querySelector('.btn:nth-child(7)');

const allButtons = document.querySelectorAll('.btn');

document.addEventListener('keydown', (event) => {

    allButtons.forEach(btn => {
        btn.classList.remove('blue');
    });

    const key = event.key.toLowerCase();

    if (event.key === 'Enter') {
        enter.classList.add('blue');
    } if (key === 's') {
        keyS.classList.add('blue');
    } if (key === 'e') {
        keyE.classList.add('blue');
    } if (key === 'o') {
        keyO.classList.add('blue');
    } if (key === 'n') {
        keyN.classList.add('blue');
    } if (key === 'l') {
        keyL.classList.add('blue');
    } if (key === 'z') {
        keyZ.classList.add('blue');
    }
});



