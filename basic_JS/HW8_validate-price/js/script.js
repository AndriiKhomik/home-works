const input = document.querySelector('.input');

function checkPrice() {
    input.addEventListener('focus', () => {
        input.addEventListener('keypress', () => {
            input.classList.add('valid');
        });
    });

    input.addEventListener('blur', () => {
        let inputValue = document.querySelector('.input').value;
        inputValue = +inputValue;

        if ((Number(inputValue < 0)) || (Number.isNaN(inputValue)) || inputValue === 0) {
            input.classList.toggle('invalid');

            removeElement(createInvalidElement());
            input.value = '';
        } else {
            input.classList.remove('valid');
            input.classList.add('valid-background');
            removeElement(createValidElement());
        }
    });
}

checkPrice();

function removeElement(elem) {
    const closeButton = document.querySelector('.close-button');
    closeButton.addEventListener('click', () => {
        elem.remove();
    });
}

function createInvalidElement() {
    const span = `
            <span class="current-price">
              Please enter correct price
              <i class="fas fa-times close-button"></i>
            </span>
        `;

    const spanElement = document.createElement('span');
    spanElement.innerHTML = span;

    document.body.append(spanElement);
    return spanElement;
}

function createValidElement() {
    const span = `
            <span class="current-price">
              Текущая цена: ${input.value} UAH
              <i class="fas fa-times close-button"></i>
            </span>
        `;

    const spanElement = document.createElement('span');
    spanElement.innerHTML = span;
    document.body.append(spanElement);
    return spanElement;
}







