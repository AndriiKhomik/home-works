const myCar = {
    color: "red",
    wheels: 4,
    engine: {
        cylinders: 4,
        size: 2.2
    }
};

const clonedCar = cloningObj(myCar);

function cloningObj(obj) {
    const cloObj = {};
    for (let i in obj) {
        if (obj[i] instanceof Object) {
            cloObj[i] = cloningObj(obj[i]);
            continue;
        }
        cloObj[i] = obj[i];
    }
    return cloObj;
}

console.log(myCar);
console.log(clonedCar);

myCar.wheels = 6;
clonedCar.wheels = 2;
clonedCar.engine.cylinders = 8;
clonedCar.engine.size = 4.0;

console.log(myCar);
console.log(clonedCar);