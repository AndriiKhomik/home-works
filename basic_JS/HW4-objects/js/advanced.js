let firstName = prompt('Please type your name');
let lastName = prompt('Please type your last name');

function createNewUser(firstName, lastName) {
    const newUser = {
        _userName: firstName,
        _userLastName: lastName,
        set userName(value) {
            if (value === null || value === '') {
                return false;
            }
            return  this._userName = value;
        },
        set userLastName(value) {
            if (value === null || value === '') {
                return false;
            }
            return  this._userLastName = value;
        },
        getLogin() {
            return this._userName.slice(0, 1).toLowerCase()+this._userLastName.toLowerCase();
        }
    };
    return newUser
}

const newUser = createNewUser(firstName, lastName);
console.log(newUser);

newUser.userName = '';
newUser.userLastName = '';

console.log(newUser);

