let firstName = prompt('Please type your name');
while (!isValid(firstName)) {
    firstName = prompt('Please type your name');
}

let lastName = prompt('Please type your last name');
while (!isValid(lastName)) {
    lastName = prompt('Please type your last name');
}

function isValid(value) {
    return (!(value === '' || value === null));
}

function createNewUser(firstName, lastName) {
    const newUser = {
        firstName,
        lastName,
        getLogin() {
            return this.firstName.slice(0, 1).toLowerCase()+this.lastName.toLowerCase();
        }
    };
    return newUser
}

const firstUser = createNewUser(firstName, lastName);
console.log(firstUser);
console.log(firstUser.getLogin());


