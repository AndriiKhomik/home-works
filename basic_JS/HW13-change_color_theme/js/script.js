const switcher = document.querySelector('.slider');
const theme = document.querySelector('.theme');
const currentTheme = localStorage.getItem('theme');

function setTheme(name){
    theme.setAttribute('data-theme', name)
    localStorage.setItem('theme', name)
}

if (currentTheme) {
    theme.setAttribute('data-theme', currentTheme)
} else {
    setTheme('light');
}

switcher.addEventListener('click', () => {
    if (theme.getAttribute('data-theme') === 'light') {
        setTheme('dark');
    } else {
        setTheme('light');
    }
});
