const employee = {
    firstName: 'Vitalii',
    lastName: 'Klichko'
};

const { firstName, lastName } = employee;

const boxer = {
    firstName,
    lastName,
    age: 45,
    salary: 3495
};

console.log(boxer);
console.log('-------------');
