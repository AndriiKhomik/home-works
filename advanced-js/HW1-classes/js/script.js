class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set setName(value) {
        if (value.length < 4) {
            console.log('Name is too shout, please check it');
            return this._name
        }
         this._name = value;
    }

    get getName() {
        return this._name
    }

    set setAge(value) {
        if (value < 18) {
            console.log('You are too young to work');
            return this._age;
        } else if (value > 60) {
            console.log('You should think about retirement');
        }
        this._age = value
    }

    get getAge() {
        return this._age
    }

    set setSalary(value) {
        if (value < 10000) {
            console.log('You should think about another job');
            this._salary = value
        }
        this._salary = value
    }

    get getSalary() {
        return this._salary
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get getSalary() {
        return this._salary * 3
    }
}

const employee = new Employee('Andrii', 34, 25000);
console.log(employee);
employee.setAge = 62;
employee.setName = 'Fred';
employee.setSalary = 9000;
console.log(employee.getName);
console.log(employee.getAge);
console.log(employee.getSalary);
console.log(employee);

const programmer = new Programmer('Jack', 25, 15000, 'english, spanish');
console.log(programmer);
console.log(programmer.getSalary);

const frontEndProgrammer = new Programmer('Stacy', 21, 44000, 'italian');
console.log(frontEndProgrammer);

const backEndProgrammer = new Programmer('Brad', 52, 68000, 'chinese, turkish');
console.log(backEndProgrammer);
console.log(backEndProgrammer.getSalary);




