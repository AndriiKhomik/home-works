const BASE_URL = 'https://ajax.test-danit.com/api/swapi';

function createList(list) {
    const ul = document.createElement(list);
    ul.classList.add('content-box');
    document.querySelector('#root').append(ul)
}

const makeRequest = url => fetch(url, {
        method: 'get'
    }).then(response => {
        if(response.ok) {
            return response.json()
        } else {
            throw new Error('Server error')
        }
    });

const request = makeRequest(`${BASE_URL}/films`)
    .then(data => {
        createList('ul');
        const listItems = document.querySelector('ul');

        const characters = data.map(({ characters, episodeId, name, openingCrawl }) => {
            const receivedCharacters = characters.map(url => makeRequest(url));
            Promise.all(receivedCharacters)
                .then(characters => {
                    const characterNames = characters.map(({ name }) => {
                        const li = document.createElement('li');
                        li.innerText = name;
                        listItems.append(li);
                        return li
                    });
                    listOfCharacters.append(...characterNames)
            });

            const container = document.createElement('li');
            const title = document.createElement('h2');
            title.innerText = `Title of the film: ${name}`;
            const episode = document.createElement('h3');
            episode.innerText = `Episode nr: ${episodeId}`;
            const description = document.createElement('p');
            description.innerText = `Short description of the episode: \n ${openingCrawl}`;
            const headerCharacters = document.createElement('h4');
            headerCharacters.innerText = 'Starring:';

            const listOfCharacters = document.createElement('ul');

             container.append(title, episode, description, headerCharacters, listOfCharacters);
            return container
        });

        listItems.prepend(...characters)
    });


















