class EstimateByIp {
  constructor() {
    this.render();
    this.getIpRequest();
    this.sendDataRequest()
  }
  createElement(el, classNames, text) {
    const element = document.createElement(el);
    element.classList.add(...classNames);
    element.innerText = text;
    return element
  }

  render() {
    this.button = this.createElement('button', ['btn', 'btn-secondary'], 'Define IP');
    this.requestButton = this.createElement('button', ['btn', 'btn-info'], 'Get data by IP');
    this.requestButton.setAttribute('disabled', true);
    this.root = document.querySelector('#root');
    this.root.append(this.button, this.requestButton)
  }

  async getIp() {
    const response = await fetch('https://api.ipify.org/?format=json');
    this.userIp = await response.json();
    this.tooltip = this.createElement('div', ['info'], `IP: ${this.userIp.ip} received`);
    this.root.append(this.tooltip);
    this.requestButton.removeAttribute('disabled');
  }

  getIpRequest() {
    this.button.addEventListener('click', () => {
      if (this.dataList) {
        this.dataList.remove();
      } if (this.tooltip) {
        this.tooltip.remove();
      }
      return this.getIp();
    })
  }

  async request() {
    const response = await fetch(`http://ip-api.com/json/${this.userIp.ip}`);
    this.responseJson = await response.json();
    this.renderUserDataByIP()
  }

  renderUserDataByIP() {
    const { country, region, city, zip } = this.responseJson;

    this.dataList = this.createElement('ul', ['list'], '');
    this.root.append(this.dataList);
    const liCountry = this.createElement('li', ['list-item'], `Your country is: ${country}`);
    const liRegion = this.createElement('li', ['list-item'], `Your region is: ${region}`);
    const liCity = this.createElement('li', ['list-item'], `Your city is: ${city}`);
    const liZip = this.createElement('li', ['list-item'], `Your ZIP code is: ${zip}`);
    this.dataList.append(
      liCountry,
      liRegion,
      liCity,
      liZip
    )
  }

  sendDataRequest() {
    this.requestButton.addEventListener('click', () => {
      if (this.dataList) {
        this.dataList.remove();
      }
      return this.request()
    })
  }
}

new EstimateByIp();

