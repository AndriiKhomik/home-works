const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

function createList() {
    const list = document.createElement('ol');
    list.classList.add('list');
    document.getElementById('root').append(list);
}

createList();

function createElement(element, value) {
    const el = document.createElement(element);
    el.textContent = value;
    document.querySelector('.list').append(el)
}

function displayedList(arr) {
    arr.map((element) => {
        // common error
        if (!element.author || !element.name || !element.price) {
            console.error('Some of the object keys are undefined')
        }
        // Specific errors
        // if (!element.author) {
        //     console.error('Author is not defined');
        //     return;
        // } if (!element.name) {
        //     console.error('Name is not defined');
        //     return;
        // } if (!element.price) {
        //     console.error('Price is not defined');
        //     return;
        // }
        else {
            createElement('li', [element.author, element.name, element.price])
        }
    })
}

displayedList(books);

