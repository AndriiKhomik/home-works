import React, {Component} from "react";
import Modal from "../Modal";
import Header from "../Header";
import ItemsList from "../ItemsList";

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';
import {getAlbums} from "../../api/requestAlbums";
import {Loader} from "../Loader";

const dataFromLC = () => {
  let data = localStorage.getItem('favorite');
  if (data === null) {
    return []
  }
  return data.split(',')
};


class App extends Component {

  state = {
    albums: [],
    isOpen: false,
    isLoading: true,
    selectedItems: dataFromLC()
  };

  componentDidMount() {
    setTimeout(() => {
      getAlbums()
        .then(albums => {
          this.setState(
            {albums})
        })
        .finally(() => {
          this.setState({
            isLoading: false
          })
        })
    }, 500)
  }

  onToggleIcon = (id) => {
    let indexArray = this.state.selectedItems;
    const uniqueIndex = indexArray.findIndex((elem) => elem === id);

    if (uniqueIndex <= -1) {
      this.setState({
        selectedItems: [
          ...indexArray,
          id
        ],
        isSelected: !this.state.isSelected
      });
    } else if (uniqueIndex >= -1) {
      this.setState({
        selectedItems: [
          ...indexArray.slice(0, uniqueIndex),
          ...indexArray.slice(uniqueIndex + 1)
        ],
        isSelected: !this.state.isSelected
      })
    }
  };

  toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen
    })
  };

  render() {
    localStorage.setItem('favorite', this.state.selectedItems);
    return (
      <>
        <Header />
        <ItemsList
          albums={this.state.albums}
          isLoading={this.state.isLoading}
          selectedItems={this.state.selectedItems}
          onToggleIcon={this.onToggleIcon}
          toggleModal={this.toggleModal}
        />
        {this.state.isLoading ? <Loader /> : null}

        <Modal
          isOpen={this.state.isOpen}
          toggleModalProp={this.toggleModal}
          actions={<>
            <button className='btn btn-secondary'
                    onClick={this.toggleModal}>Yes
            </button>
            <button className='btn btn-secondary'
                    onClick={this.toggleModal}>Buy Later
            </button>
          </>}>

          <h2 className='title'>Do you want to buy this album?
            <span onClick={this.toggleModal}>
              <i className="fas fa-times"></i>
            </span>
          </h2>
          <div className='text'>If you have some doubts, please add it to favorite and make decision later</div>
        </Modal>
      </>
    )
  }
}

export default App;
