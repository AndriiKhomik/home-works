import React, { Component } from "react";

import './Item.scss';

class Item extends Component {

  handleModal = (e) => {
    e.preventDefault();
    this.props.toggleModal()
  };

  handleIcon = (e) => {
    e.preventDefault();
    this.props.onToggleIcon()
  };

  render() {
    const { album, band, price, release, img, id, selectedItems } = this.props;

    return (
      <>
        <td><img src={img} alt='album'/></td>
        <td>{band}</td>
        <td>{album}</td>
        <td className='price'>{price} <span>$</span> </td>
        <td>{release}</td>
        <td><a href='#'
               onClick={this.handleIcon}>
          {selectedItems.includes(id) ? <i className="fas fa-star"></i> : <i className="far fa-star"></i>}
        </a></td>
        <td><a href='#'
          onClick={this.handleModal}>
          <i className="fas fa-shopping-cart"></i>
        </a></td>
      </>
    )
  }
}

export default Item;
