import React, { Component } from "react";

import './Header.scss';

class Header extends Component {
  render() {
    return (
      <div className='header'>
        <div className='container header__container'>
          <a href="https://www.billboard.com/">Music Store</a>
          <h2 className='header__title text-center'>
            You can buy top albums regarding <span>billboard</span> chart
          </h2>
          <a href='#'>
            <i className="fas fa-shopping-cart header__cart"></i><span>0</span>
          </a>
        </div>
      </div>
    )
  }
}

export default Header;