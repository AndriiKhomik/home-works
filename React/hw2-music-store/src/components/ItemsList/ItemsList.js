import React, { Component } from "react";
import Item from "../Item";

import './ItemsList.scss'

class ItemsList extends Component {

  render() {
    const { toggleModal, onToggleIcon } = this.props;
    const albums = this.props.albums;
    const albumItem = albums.map(albumsList => {
      const { id, ...albumProps } = albumsList;

      return (
        <tr key={id} className='item'>
          <Item
            {...albumProps}
            id={id}
            selectedItems={this.props.selectedItems}
            onToggleIcon={() => onToggleIcon(id)}
            toggleModal={() => toggleModal(id)}
          />
        </tr>
      )
    });

    return (
      <div className='container'>
        <h4 className='text-center albums__title'>Top rated</h4>
        <table className='table'>
          <thead>
          <tr>
            <td></td>
            <td>Band</td>
            <td>Album</td>
            <td>Price</td>
            <td>Release year</td>
            <td>Like</td>
            <td>Buy</td>
          </tr>
          </thead>
            <tbody>
              {albumItem}
            </tbody>
        </table>
    </div>
    )
  }
}

export default ItemsList;