import React, {Component} from "react";
import Button from "../Button";
import Modal from "../Modal";

class App extends Component {

  state = {
    isOpenFirst: false,
    isOpenSecond: false
  };

  toggleModalFirst = () => {
    this.setState({
      isOpenFirst: !this.state.isOpenFirst
    })
  };

  toggleModalSecond = () => {
    this.setState({
      isOpenSecond: !this.state.isOpenSecond
    })
  };

  render() {
    return (
      <>
        <Button onClick={this.toggleModalFirst}
                title='Open first modal'
                styleBtn='red'/>
        <Button onClick={this.toggleModalSecond}
                title='Open second modal'
                styleBtn='green'/>

        <Modal actions={<>
          <button className='btn'
                  onClick={this.toggleModalFirst}>Ok
          </button>
          <button className='btn'
                  onClick={this.toggleModalFirst}>Cancel
          </button>
        </>}
               isOpen={this.state.isOpenFirst}
               toggleModalProp={this.toggleModalFirst}
        >
          <h2 className='title'>Do you want to delete file? <span onClick={this.toggleModalFirst}>X</span></h2>
          <div className='text'>This file contains virus, please delete it</div>
        </Modal>

        <Modal actions={<>
          <button className='btn'
                  onClick={this.toggleModalSecond}>yes
          </button>
          <button className='btn'
                  onClick={this.toggleModalSecond}>no
          </button>
        </>}
               isOpen={this.state.isOpenSecond}
               toggleModalProp={this.toggleModalSecond}
        >
          <h2 className='title'>Are you 18 year old?<span onClick={this.toggleModalSecond}>X</span></h2>
          <div className='text'>This content is allowed for persons older than 18 years</div>
        </Modal>
      </>
    )
  }
}

export default App;
