import React, { Component } from "react";
import './Button.scss'

class Button extends Component {

  handleClick = (e) => {
    this.props.onClick(e)
  };

  render() {
    const { styleBtn, title } = this.props;
    return (
      <button onClick={this.handleClick}
        className='btn'
        style={ {backgroundColor: styleBtn} }>{title}</button>
    )
  }
}

export default Button;