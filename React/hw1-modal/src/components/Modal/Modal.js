import React, { Component } from "react";
import './Modal.scss';

class Modal extends Component {

  render() {
    const { isOpen, toggleModalProp, children, actions } = this.props;
    return (
      <div  onClick={toggleModalProp} className={isOpen ? 'modalContainer active' : 'modalContainer'}>
        <div className='modal' onClick={(e) => e.stopPropagation()}>
          {children}
          {actions}
        </div>
      </div>
    )
  }
}

export default Modal;